import numpy as np
import pandas as pd
import re
import json
import pymysql.cursors
from scipy import stats
import ast
import time
from subprocess import call

################################
# find items matching by lat,lng, (brand) name
# by comparing candidates_to_update.csv with DB
#
def query_POI_with_complex():
    with conn_layer.cursor() as cursor:
        query = "select distinct s.pid as store_id, s.name as store_name, s.branch_name as store_branch_name, s.`category` as category, s.category_code as category_code, c.`name` as complex_name, c.branch_name as complex_branch_name, s.lat as lat, s.lng as lng, s.addr as addr, s.addr_road as addr_road, s.phone as phone, s.`created_at` as created_at, s.state as state \
	                from layer_map as l \
                    inner join complex as c \
                    inner join store as s \
                    where l.layers = '203' \
                    and l.id1 = s.pid \
                    and l.id2 = c.cid \
                    and s.created_at between date_format(now() - interval 1 MONTH, '%%Y-%%m-01 00:00:00') and date_format(last_day(now() - interval 1 month), '%%Y-%%m-%%d 23:59:59') \
                    order by s.created_at DESC"
        df_query_result = pd.read_sql(query, conn_layer, params={})
        return df_query_result

def query_POI_without_complex():
    with conn_layer.cursor() as cursor:
        query = "select s.pid as store_id, s.name as store_name, s.branch_name as store_branch_name, s.`category` as category, s.category_code as category_code, '' as complex_name, '' as complex_branch_name, s.lat as lat, s.lng as lng, s.addr as addr, s.addr_road as addr_road, s.phone as phone, s.`created_at` as created_at, s.state as state \
	                from store as s \
                    where s.pid NOT IN ( \
                        select distinct s.pid \
                            from layer_map as l \
                            inner join complex as c \
                            inner join store as s \
                            where l.layers = '203' \
                            and l.id1 = s.pid \
                            and l.id2 = c.cid  \
                    ) \
                    and s.created_at between date_format(now() - interval 1 MONTH, '%%Y-%%m-01 00:00:00') and date_format(last_day(now() - interval 1 month), '%%Y-%%m-%%d 23:59:59') \
                    order by s.created_at DESC"
        df_query_result = pd.read_sql(query, conn_layer, params={})
        return df_query_result

def query_category():
    with conn_data.cursor() as cursor:
        query = "select code as category_code, category_1_kor, category_2_kor, category_3_kor, category_4_kor, category_1_eng, category_2_eng, category_3_eng, category_4_eng \
	                from loplat_category"
        df_query_result = pd.read_sql(query, conn_data, params={})
        return df_query_result



cred_layer = pd.read_json('../cred/cred_loplat_layer_replica_readonly.json', typ='series')
cred_data = pd.read_json('../cred/cred_loplatdata.json', typ='series')


# POI
conn_layer = pymysql.connect(host=cred_layer.host,
            user=cred_layer.user,
            password=cred_layer.pwd,
            db=cred_layer.db,
            charset='utf8mb4')
try:
    print("Start")
    df_POI_with_complex = query_POI_with_complex()
    print("POI with complex downloaded")
    df_POI_without_complex = query_POI_without_complex()
    print("POI without complex downloaded")

finally:
    conn_layer.close()


# category
conn_data = pymysql.connect(host=cred_data.host,
            user=cred_data.user,
            password=cred_data.pwd,
            db=cred_data.db,
            charset='utf8mb4')
try:
    print("Start")
    df_category = query_category()
    print("Category downloaded")
finally:
    conn_data.close()


################################
# build final results
#

df_merged_with_complex = pd.merge(df_POI_with_complex, df_category, on='category_code', how='inner')
df_merged_without_complex = pd.merge(df_POI_without_complex, df_category, on='category_code', how='inner')
print(df_merged_with_complex.columns)
print(df_merged_without_complex.columns)
df_merged = pd.concat([df_merged_with_complex, df_merged_without_complex])

df_merged = df_merged[['store_id', 'store_name', 'store_branch_name', 'category',
       'category_code', 'category_1_kor','category_2_kor', 'category_3_kor', 'category_4_kor', 'category_1_eng',
       'category_2_eng', 'category_3_eng', 'category_4_eng', 'lat', 'lng','addr', 'addr_road', 'phone', 'created_at', 'complex_name', 'complex_branch_name', 'state']]

import datetime
today = datetime.date.today()
first = today.replace(day=1)
lastMonth = first -datetime.timedelta(days=1)
_previous_month_str = str(lastMonth.strftime('%Y%m'))

_filename = "./results/POI_SH_" + _previous_month_str + ".csv"
df_merged.to_csv(_filename)
print(df_merged.shape)
print(df_merged.head())





from slacker import Slacker

token = "xoxp-5085688938-403144057697-479211519202-b4eee0ecdd0d8e0643da1c6958cef434"
slack = Slacker(token)

_message = "Counts\n" + df_merged.shape[0] 

attachments_dict = dict()
attachments_dict['pretext'] = "POI acquired in previous month(" + _previous_month_str + ") is now ready."
attachments_dict['title'] = "Link to the file"
#attachments_dict['title_link'] = "http://www.loplat.com"
attachments_dict['fallback'] = "Oops"
attachments_dict['text'] = _message
attachments_dict['mrkdwn_in'] = ["text", "pretext"]  # 마크다운을 적용시킬 인자들을 선택합니다.
attachments = [attachments_dict]


slack.chat.post_message(channel='#data_analysis', text=None, attachments=attachments, as_user=False)

